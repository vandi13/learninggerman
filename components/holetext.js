import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useAlert } from "react-alert";

export default function Holetext(props) {
  const alert = useAlert();
  const [html, setHtml] = useState("<span></span>");
  const [done, setDone] = useState(false);

  const verify = () => {
    let should = props.task.holetext_answers;
    let correct = true;
    for (let i = 0; i < should.length; i++) {
      let element = document.getElementById("holetextinput" + i);
      if (element.value.toLowerCase() !== should[i].toLowerCase()) {
        correct = false;
      }
    }
    if (correct) {
      props.handleCorrectAnswer();
    } else {
      alert.show("Answer not correct", { type: "error" });
    }
  };

  const renderHoletext = () => {
    let splittedText = props.task.text.split("*");
    let pHtml = "";
    splittedText.map((pText, i) => {
      if (i > 0) {
        pHtml +=
          "<input type='text' id='holetextinput" +
          (i - 1) +
          "' style='width:80px;'/>";
      }
      pHtml += "<span>" + pText + "</span>";
    });
    setHtml(pHtml);
    setDone(true);
  };
  useEffect(() => {
    renderHoletext();
  }, [props.task]);
  if (!done) return <p>Loading</p>;
  return (
    <>
      <p dangerouslySetInnerHTML={{ __html: html }}></p>
      <div
        className="d-grid gap-2"
        style={{ position: "absolute", bottom: "30px", width: "90%" }}
      >
        <Button variant="primary" onClick={() => verify()}>
          Verify
        </Button>
        <span style={{ fontSize: "10pt" }}>
          +{props.task.dirham} Dirham if correct
        </span>
      </div>
    </>
  );
}
