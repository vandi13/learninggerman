import { API } from "aws-amplify";
import React, { useEffect } from "react";
import { useState } from "react";
import { Col, Row, Spinner, Button } from "react-bootstrap";
import Holetext from "./holetext";

export default function Task(props) {
  const [task, setTask] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (props.taskId === undefined) return;
    API.get("rihabapi", "/tasks/" + props.taskId).then((result) => {
      setTask(result.body);
      console.log(result.body);
      setLoading(false);
    });
  }, [props.taskId]);

  const handleCorrectAnswer = async () => {
    API.post("rihabapi", "/tasks", {
      body: {
        taskid: task.id,
      },
    }).then((result) => {
      props.handleCorrectAnswer();
    });
  };

  if (loading) {
    return <Spinner animation="border" />;
  }
  return (
    <>
      <Row>
        <Col style={{ fontSize: "12px" }}>{task.hint}</Col>
      </Row>
      <Row>
        <Col className="pt-5">
          {task.type === "holetext" && (
            <Holetext task={task} handleCorrectAnswer={handleCorrectAnswer} />
          )}
        </Col>
      </Row>
    </>
  );
}
