/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_LESSONS_ARN
	STORAGE_LESSONS_NAME
	STORAGE_LESSONS_STREAMARN
	STORAGE_MONEY_ARN
	STORAGE_MONEY_NAME
	STORAGE_MONEY_STREAMARN
	STORAGE_TASKS_ARN
	STORAGE_TASKS_NAME
	STORAGE_TASKS_STREAMARN
Amplify Params - DO NOT EDIT */

var express = require("express");
var bodyParser = require("body-parser");
var awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var documentClient = new AWS.DynamoDB.DocumentClient();
// declare a new express app
var app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

/**********************
 * Example get method *
 **********************/

app.get("/tasks", function (req, res) {
  // Add your code here
  res.json({ success: "get call succeed!", url: req.url });
});

app.get("/tasks/:taskid", async function (req, res) {
  let taskid = req.params.taskid;
  let task = await documentClient
    .get({
      TableName: process.env.STORAGE_TASKS_NAME,
      Key: { id: parseInt(taskid) },
    })
    .promise();

  res.json({ success: "true", url: req.url, body: task.Item });
});

/****************************
 * Example post method *
 ****************************/

app.post("/tasks", async function (req, res) {
  // Add your code here
  let taskid = parseInt(req.body.taskid);
  let query = await documentClient
    .get({
      TableName: process.env.STORAGE_TASKS_NAME,
      Key: { id: parseInt(taskid) },
    })
    .promise();
  let dirham = parseInt(query.Item.dirham);

  await documentClient
    .update({
      TableName: process.env.STORAGE_MONEY_NAME,
      Key: { id: "amount" },
      UpdateExpression: "set amount = amount + :dirham",
      ExpressionAttributeValues: {
        ":dirham": dirham,
      },
    })
    .promise();

  res.json({ success: "true", url: req.url, body: req.body });
});

app.post("/tasks/*", function (req, res) {
  // Add your code here
  res.json({ success: "post call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example put method *
 ****************************/

app.put("/tasks", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

app.put("/tasks/*", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example delete method *
 ****************************/

app.delete("/tasks", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.delete("/tasks/*", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.listen(3000, function () {
  console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
