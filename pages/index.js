import { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoins } from "@fortawesome/free-solid-svg-icons";
import { API } from "aws-amplify";
import React from "react";
import styled from "styled-components";
import Link from "next/link";

const ContentContainer = styled(Container)`
  background-color: white;
  width: 95%;
  height: calc(100% - 60px);
  border-radius: 10px;
  color: black;
`;
const ButtonWrapper = styled.div``;

export default function Home() {
  const [money, setMoney] = useState(0);
  useEffect(() => {
    API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
  }),
    [API];
  return (
    <>
      <Container style={{ height: "50px" }}>
        <Row>
          <Col
            className="d-flex justify-content-center"
            style={{ paddingTop: "10px" }}
          >
            <p>
              <FontAwesomeIcon icon={faCoins} /> Dirham: {money}
            </p>
          </Col>
        </Row>
      </Container>
      <ContentContainer>
        <Row>
          <Col className="pt-5 d-flex justify-content-center">
            <h1>Main Menu</h1>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col>
            <ButtonWrapper className="d-grid gap-2">
              <Link href="/inventory">
                <Button variant="primary" size="lg">
                  Inventory
                </Button>
              </Link>
              <Link href="/shop">
                <Button variant="primary" size="lg" className="mt-3">
                  Shop
                </Button>
              </Link>
              <Link href="/lessons">
                <Button variant="dark" size="lg" className="mt-3">
                  Lessons
                </Button>
              </Link>
            </ButtonWrapper>
          </Col>
        </Row>
      </ContentContainer>
    </>
  );
}
