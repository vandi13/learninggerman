import { faArrowLeft, faCoins } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API } from "aws-amplify";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Spinner, Table } from "react-bootstrap";
import styled from "styled-components";

const ContentContainer = styled(Container)`
  background-color: white;
  width: 95%;
  height: calc(100% - 60px);
  border-radius: 10px;
  color: black;
  a,
  a:visited,
  a:hover,
  a:active,
  a:link {
    text-decoration: none;
    color: black;
  }
`;
const ButtonWrapper = styled.div``;

export default function Inventory() {
  const [money, setMoney] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
    API.get("rihabapi", "/items").then((result) => {
      let items = result.body.filter((item) => item.amount > 0);
      setItems(items);
      setLoading(false);
    });
  }, [API]);
  if (loading) {
    return (
      <Container
        fluid
        className="h-100 d-flex align-items-center justify-content-center"
      >
        <Spinner animation="border" />
      </Container>
    );
  }
  return (
    <>
      <Container style={{ height: "50px" }}>
        <Row>
          <Col
            className="d-flex justify-content-center"
            style={{ paddingTop: "10px" }}
          >
            <p>
              <FontAwesomeIcon icon={faCoins} /> Dirham: {money}
            </p>
          </Col>
        </Row>
      </Container>
      <ContentContainer>
        <Row>
          <Col>
            <Link href="/">
              <a>
                <FontAwesomeIcon icon={faArrowLeft} /> Go back
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col className="pt-2 d-flex justify-content-center">
            <h1>Inventory:</h1>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col>
            {items.length === 0 && (
              <p>You don't own anything. Buy something in the store first</p>
            )}
            {items.length > 0 && (
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Item</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  {items.map((item, i) => {
                    return (
                      <tr key={`ir${i}`}>
                        <td>{item.item_name}</td>
                        <td>{item.amount}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            )}
          </Col>
        </Row>
      </ContentContainer>
    </>
  );
}
