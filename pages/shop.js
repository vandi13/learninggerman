import { faArrowLeft, faCoins } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API } from "aws-amplify";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Spinner, Table } from "react-bootstrap";
import styled from "styled-components";
import { useAlert } from "react-alert";

const ContentContainer = styled(Container)`
  background-color: white;
  width: 95%;
  height: calc(100% - 60px);
  border-radius: 10px;
  color: black;
  a,
  a:visited,
  a:hover,
  a:active,
  a:link {
    text-decoration: none;
    color: black;
  }
`;
const ButtonWrapper = styled.div``;

export default function Shop() {
  const alert = useAlert();

  const [money, setMoney] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
    API.get("rihabapi", "/items").then((result) => {
      let items = result.body;
      setItems(items);
      setLoading(false);
    });
  }, [API]);

  const buyItem = async (item_name, price) => {
    if (price > money) {
      alert.show("Not enough money", { type: "error" });
      return;
    }
    API.post("rihabapi", "/items", {
      body: {
        itemname: item_name,
      },
    }).then((result) => {
      if (result.success === "true") {
        alert.show("Success", { type: "success" });
      }
    });
  };

  if (loading) {
    return (
      <Container
        fluid
        className="h-100 d-flex align-items-center justify-content-center"
      >
        <Spinner animation="border" />
      </Container>
    );
  }

  return (
    <>
      <Container style={{ height: "50px" }}>
        <Row>
          <Col
            className="d-flex justify-content-center"
            style={{ paddingTop: "10px" }}
          >
            <p>
              <FontAwesomeIcon icon={faCoins} /> Dirham: {money}
            </p>
          </Col>
        </Row>
      </Container>
      <ContentContainer>
        <Row>
          <Col>
            <Link href="/">
              <a>
                <FontAwesomeIcon icon={faArrowLeft} /> Go back
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col className="pt-2 d-flex justify-content-center">
            <h1>Shop:</h1>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                {items.map((item, i) => {
                  return (
                    <tr key={`ir${i}`} className="align-middle">
                      <td>{item.item_name}</td>
                      <td>{item.price} Dirham</td>
                      <td>
                        <Button
                          variant="primary"
                          onClick={() => buyItem(item.item_name, item.price)}
                        >
                          Buy
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </ContentContainer>
    </>
  );
}
