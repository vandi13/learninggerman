import { faArrowLeft, faCoins } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API } from "aws-amplify";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Container,
  ProgressBar,
  Row,
  Spinner,
  Table,
} from "react-bootstrap";
import styled from "styled-components";
import { useRouter } from "next/router";
import Task from "../../components/task";

const ContentContainer = styled(Container)`
  background-color: white;
  width: 95%;
  height: calc(100% - 60px);
  border-radius: 10px;
  color: black;
  a,
  a:visited,
  a:hover,
  a:active,
  a:link {
    text-decoration: none;
    color: black;
  }
`;
const ButtonWrapper = styled.div``;

export default function Lesson() {
  const router = useRouter();
  const { id } = router.query;

  const [money, setMoney] = useState(0);
  const [lesson, setLesson] = useState([]);
  const [loading, setLoading] = useState(true);
  const [tasks, setTasks] = useState([]);
  const [currentTask, setCurrentTask] = useState(0);
  const [showEndScreen, setShowEndScreen] = useState(false);

  useEffect(async () => {
    if (id === undefined) return;
    await API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
    await API.get("rihabapi", "/lessons/" + id).then((result) => {
      setLesson(result.body);
      setTasks(result.body.tasks);
      console.log(result.body);
    });
    setLoading(false);
  }, [id]);

  const handleCorrectAnswer = () => {
    console.log(currentTask);
    console.log(tasks.length);
    API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
    if (currentTask + 1 === tasks.length) {
      setShowEndScreen(true);
    } else {
      setCurrentTask(currentTask + 1);
    }
  };

  if (loading) {
    return (
      <Container
        fluid
        className="h-100 d-flex align-items-center justify-content-center"
      >
        <Spinner animation="border" />
      </Container>
    );
  }

  return (
    <>
      <Container style={{ height: "50px" }}>
        <Row>
          <Col
            className="d-flex justify-content-center"
            style={{ paddingTop: "10px" }}
          >
            <p>
              <FontAwesomeIcon icon={faCoins} /> Dirham: {money}
            </p>
          </Col>
        </Row>
      </Container>
      <ContentContainer>
        <Row>
          <Col>
            <Link href="/lessons">
              <a>
                <FontAwesomeIcon icon={faArrowLeft} /> Go back
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center">
            <b>{lesson.name}</b>
          </Col>
        </Row>
        <Row>
          <Col className="pt-3">
            <ProgressBar
              now={((currentTask + 1) / tasks.length) * 100}
              animated
            />
          </Col>
        </Row>
        {showEndScreen ? (
          <Row>
            <Col className="text-center">
              <h1>Lesson done</h1>
              <h2>I'm impressed</h2>
              <img src="/impressed.png" style={{ maxWidth: "80%" }} />
            </Col>
          </Row>
        ) : (
          <Row>
            <Col className="pt-3">
              <Task
                taskId={tasks[currentTask]}
                handleCorrectAnswer={handleCorrectAnswer}
              />
            </Col>
          </Row>
        )}
      </ContentContainer>
    </>
  );
}
