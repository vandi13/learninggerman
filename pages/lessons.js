import { faArrowLeft, faCoins } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API } from "aws-amplify";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Spinner, Table } from "react-bootstrap";
import styled from "styled-components";

const ContentContainer = styled(Container)`
  background-color: white;
  width: 95%;
  height: calc(100% - 60px);
  border-radius: 10px;
  color: black;
  a,
  a:visited,
  a:hover,
  a:active,
  a:link {
    text-decoration: none;
    color: black;
  }
`;
const ButtonWrapper = styled.div``;

export default function Lessons() {
  const [money, setMoney] = useState(0);
  const [lessons, setLessons] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(async () => {
    await API.get("rihabapi", "/money").then((result) => {
      setMoney(result.body.amount);
    });
    await API.get("rihabapi", "/lessons").then((result) => {
      setLessons(result.body);
    });
    setLoading(false);
  }, [API]);
  if (loading) {
    return (
      <Container
        fluid
        className="h-100 d-flex align-items-center justify-content-center"
      >
        <Spinner animation="border" />
      </Container>
    );
  }
  return (
    <>
      <Container style={{ height: "50px" }}>
        <Row>
          <Col
            className="d-flex justify-content-center"
            style={{ paddingTop: "10px" }}
          >
            <p>
              <FontAwesomeIcon icon={faCoins} /> Dirham: {money}
            </p>
          </Col>
        </Row>
      </Container>
      <ContentContainer>
        <Row>
          <Col>
            <Link href="/">
              <a>
                <FontAwesomeIcon icon={faArrowLeft} /> Go back
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col className="pt-4 d-flex justify-content-center">
            <h1>Choose a lesson:</h1>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col>
            <div className="d-grid gap-2">
              {lessons.map((lesson, i) => {
                return (
                  <Link href={`/lessons/${lesson.id}`} key={`lsn${i}`}>
                    <Button
                      variant="primary"
                      size="lg"
                      className={i > 0 ? "mt-3" : ""}
                    >
                      {lesson.name}
                    </Button>
                  </Link>
                );
              })}
            </div>
          </Col>
        </Row>
      </ContentContainer>
    </>
  );
}
